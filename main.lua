--Joc per al Hackathon de Barcelona MOBILEAPPS dintre de BcnDevCon 2012
--Autors:
--			-Carlos Solé			@ohhmiosole
--			-Alexandre Chimeno		@achimeno
--
require "box2d"

--setting up some configurations
application:setOrientation(conf.orientation)
application:setLogicalDimensions(conf.width, conf.height)
application:setScaleMode(conf.scaleMode)
application:setFps(conf.fps)
application:setKeepAwake(conf.keepAwake)
--Hola
--get new dimensions
conf.width = application:getContentWidth()
conf.height = application:getContentHeight()

transition = SceneManager.fade

--loading application settings
sets = dataSaver.loadValue("sets")
--if sets not define (first launch)
--define defaults
if(not sets) then
	sets = {}
	sets.sounds = true
	sets.music = true
	sets.level = 1
	sets.coins = 0
	sets.armes = {}
	dataSaver.saveValue("sets", sets)
end

fonts = {}
fonts.Deathhead100 = TTFont.new("fonts/Deathhead.ttf", 50, false)
fonts.Deathhead50 = TTFont.new("fonts/Deathhead.ttf", 25, false)
fonts.Deathhead30 = TTFont.new("fonts/Deathhead.ttf", 15, false)

--background music
music =  {}
music.theme = Sound.new("sounds/barn-beat.mp3")

--turn music on
music.on = function()
	if not music.channel then
		music.channel = music.theme:play(0,1000)
		sets.music = true
		dataSaver.saveValue("sets", sets)
	end
end

--turn music off
music.off = function()
	if music.channel then
		music.channel:stop()
		music.channel = nil
		sets.music = false
		dataSaver.saveValue("sets", sets)
	end
end

--play music if enabled
if sets.music then
	music.channel = music.theme:play(0,1000)
end


--sounds
sounds = {}

--load all your sounds here
--after that you can simply play them as 
sounds.hit = Sound.new("sounds/click.wav")
sounds.chaching = Sound.new("sounds/Cha_Ching.mp3")
sounds.blast = Sound.new("sounds/Blast.mp3")
sounds.fail = Sound.new("sounds/AAAGH1.WAV")
sounds.magic = Sound.new("sounds/magic-chime-01.mp3")

--turn sounds on
sounds.on = function()
	sets.sounds = true
	dataSaver.saveValue("sets", sets)
end

--turn sounds off
sounds.off = function()
	sets.sounds = false
	dataSaver.saveValue("sets", sets)
end

--play sounds
sounds.play = function(sound)
	--check if sounds enabled
	if sets.sounds and sounds[sound] then
		sounds[sound]:play()
	end
end

--ducksboard
ducksboard = {}
ducksboard.guardians = Ducksboard.new(95804)
ducksboard.comptador = Ducksboard.new(95788)


--ducksboardComptador:push("{\"value\": 2000}")
--ducksboardComptador:push("{\"delta\": 1}")
--ducksboardComptador:push("{\"delta\": -1}")


--define scenes
sceneManager = SceneManager.new({
	--splashstudio
	["presentacio"] = presentacio,
	--menu
	["menu"] = menu,
	--city selector
	["cityselector"] = cityselector,
	--game
	["game"] = game,
	--Comprar armament
	["botiga"] = botiga
})
--add manager to stage
stage:addChild(sceneManager)

--start start scene
sceneManager:changeScene("presentacio", 0, SceneManager.moveFromTop, easing.outBack)