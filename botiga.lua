--MENU
botiga = gideros.class(Sprite)

botiga.ESCUT_GROC = 1
botiga.ESCUT_RED = 2
botiga.ESCUT_VERD = 3
botiga.LANZA_MISILES = 4
botiga.TANK = 5
botiga.BOMBEROS = 6

botiga.preus = {}
botiga.preus[botiga.ESCUT_GROC] = 5	
botiga.preus[botiga.ESCUT_RED] = 50
botiga.preus[botiga.ESCUT_VERD] = 500
botiga.preus[botiga.LANZA_MISILES] = 5
botiga.preus[botiga.TANK] = 1000
botiga.preus[botiga.BOMBEROS] = 10000

botiga.textures = {}
botiga.textures[botiga.ESCUT_GROC] = Texture.new("images/icona_escut_groc.png")	
botiga.textures[botiga.ESCUT_RED] = Texture.new("images/icona_escut_red.png")
botiga.textures[botiga.ESCUT_VERD] = Texture.new("images/icona_escut_verd.png")
botiga.textures[botiga.LANZA_MISILES] = Texture.new("images/launcher.png")
botiga.textures[botiga.TANK] = Texture.new("images/thumb-tank.png")
botiga.textures[botiga.BOMBEROS] = Texture.new("images/fire-engine-icon.png")

function botiga:init()
	application:setBackgroundColor(0x000000)
	
		--Background
	local background_texture = Texture.new("images/neon_earth.jpg")
	local background = Bitmap.new(background_texture)
	background:setScale(1)
	self:addChild(background)
	background:setPosition((application:getContentWidth()-background:getWidth())/2, (application:getContentHeight()-background:getHeight()))


	--Comptador punts
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(20,10)
	self:addChild(coin)
	
	local label = TextField.new(fonts.Deathhead50 , sets.coins)
	label:setPosition(50,30)
	label:setTextColor(0xffffff)
	self:addChild(label)
	botiga.labelcoins = label
	
	--Escuts
	local label = TextField.new(fonts.Deathhead50, "Escuts")
	label:setTextColor(0xffffff)
	label:setPosition(30, 90)	
	self:addChild(label)
	
	self:dibuixaObjecte(botiga.ESCUT_GROC, 50, 100)
	self:dibuixaObjecte(botiga.ESCUT_RED, 130, 100)
	self:dibuixaObjecte(botiga.ESCUT_VERD, 210, 100)
		
	--Armes
	local label = TextField.new(fonts.Deathhead50, "Armes")
	label:setTextColor(0xffffff)
	label:setPosition(30, 200)	
	self:addChild(label)
	
	self:dibuixaObjecte(botiga.TANK, 50, 220)
	self:dibuixaObjecte(botiga.LANZA_MISILES, 100, 300, 0.3)	
	self:dibuixaObjecte(botiga.BOMBEROS, 210, 220, 0.6)

	
	--Botons a menu i joc
	local label = TextField.new(fonts.Deathhead100, "Continuar")
	--label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("game", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	button2Game:setPosition((conf.width-label:getWidth())/2, 400)	
	self:addChild(button2Game)
	
	--Comprar crdits
	local label = TextField.new(fonts.Deathhead50, "Comprar credits")
	label:setTextColor(0xffff00)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			self:mostraPreusCredits()
		end
	, button)
	button2Game:setPosition((conf.width-label:getWidth())/2+60, 30)	
	self:addChild(button2Game)
	self.comprarcredits = label
	
	local label = TextField.new(fonts.Deathhead100, "Menu principal")
	label:setTextColor(0x0006fd)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("menu", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	button2Game:setPosition((conf.width-label:getWidth())/2, 450)	
	self:addChild(button2Game)
	
	--Preus credits
	self:initPreusCredits()
	

	self:addEventListener("enterBegin", self.onTransitionInBegin, self)
	self:addEventListener("enterEnd", self.onTransitionInEnd, self)
	self:addEventListener("exitBegin", self.onTransitionOutBegin, self)
	self:addEventListener("exitEnd", self.onTransitionOutEnd, self)
end

function botiga:onTransitionInBegin()
	print("botiga - enter begin")
end

function botiga:onTransitionInEnd()
	print("botiga - enter end")
end

function botiga:onTransitionOutBegin()
	print("botiga - exit begin")
end

function botiga:onTransitionOutEnd()
	print("botiga - exit end")
end

function botiga:dibuixaObjecte(objecte, x, y, scale)
	local texture = botiga.textures[objecte]	
	local button = Button.new(Bitmap.new(texture), Bitmap.new(texture))
	button:setPosition(x, y)	
	button:addEventListener("click",
		function()
			if(botiga.preus[objecte]>sets.coins) then
				GTween.new(self.comprarcredits, 0.5, {x=-70, y= 100, scaleX=1.5, scaleY=1.5}, {delay=0, ease = easing.outBack, dispatchEvents = false})
				GTween.new(self.comprarcredits, 0.5, {x=0, y=0, scaleX=1, scaleY=1}, {delay=0.5, ease = easing.outBack, dispatchEvents = false})
			else
				if(sets.armes[objecte]) then
					--Ja tens l'arma
					
					--Sound 
					
				else
					--Sound caixa registradora
					sounds.play("chaching")
					
					--Restar pasta
					sets.coins = sets.coins - botiga.preus[objecte]
					sets.armes[objecte] = true

					-- resta coins
					botiga.labelcoins:setText(sets.coins)
					
					--Xapusa
					sceneManager:changeScene("botiga", 0, SceneManager.moveFromTop, easing.outBack)
				end
			end
		end
	)
	if(scale) then
		button:setScale(scale)
	end

	self:addChild(button)
	self:setPreu(botiga.preus[objecte],button:getX()+button:getWidth()/2, y+button:getHeight())
	if(sets.armes[objecte]) then
		button:setAlpha(0.5)
		local label = TextField.new(fonts.Deathhead30 , "Comprat")
		label:setPosition(0,25)
		label:setTextColor(0xffffff)
		button:addChild(label)
	end
end

function botiga:setPreu(preu, x, y)
	local label = TextField.new(fonts.Deathhead50 , preu)
	if(preu>sets.coins) then
		label:setTextColor(0xff0000)
	else
		label:setTextColor(0xffffff)
	end
	
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(label:getWidth(),-20)
	label:addChild(coin)
	
	label:setPosition(x-label:getWidth()/2,y)
	self:addChild(label)
end

function botiga:initPreusCredits()
	local fons = Shape.new()
	fons:setFillStyle(Shape.SOLID, 0x000000, 0.7) 
	fons:moveTo(0,0)
	fons:lineTo(conf.width, 0)
	fons:lineTo(conf.width, 600)
	fons:lineTo(0, 600)
	fons:lineTo(0,0)
	fons:endPath()
	
	local label = TextField.new(fonts.Deathhead50 , "1$    1000")
	label:setTextColor(0xffffff)
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(label:getWidth(),-20)
	label:addChild(coin)	
	label:setPosition(100,100)
	fons:addChild(label)

	local label = TextField.new(fonts.Deathhead50 , "5$    10000")
	label:setTextColor(0xffffff)
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(label:getWidth(),-20)
	label:addChild(coin)	
	label:setPosition(80,150)
	fons:addChild(label)
	
	
	local label = TextField.new(fonts.Deathhead50 , "10$    50000")
	label:setTextColor(0xffffff)
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(label:getWidth(),-20)
	label:addChild(coin)	
	label:setPosition(70,200)
	fons:addChild(label)
	
	local label = TextField.new(fonts.Deathhead50 , "20$    500000")
	label:setTextColor(0xffffff)
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(label:getWidth(),-20)
	label:addChild(coin)	
	label:setPosition(50,250)
	fons:addChild(label)


	fons:setPosition(0, -1000)
	
	fons:addEventListener(Event.MOUSE_DOWN,
		function()
			botiga.preuscredits:setPosition(0, -1000)
		end
	)
	fons:addEventListener(Event.TOUCHES_BEGIN,
		function()
			botiga.preuscredits:setPosition(0, -1000)
		end
	)

	self:addChild(fons)
	botiga.preuscredits = fons
end

function botiga:mostraPreusCredits()
	botiga.preuscredits:setPosition(0,0)
end

