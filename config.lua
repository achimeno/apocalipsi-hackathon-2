--global conf object stores different template configurations
conf = {
	--set app's orientation
	orientation = Stage.PORTRAIT,
	--default transition for scene manager
	transition = SceneManager.flipWithFade,
	--default easing used in template
	easing = easing.outBack,
	--defauly texture fitlering
	textureFilter = true,
	--scaling mode
	scaleMode = "letterbox",
	--keep application awake
	keepAwake = true,
	--logical width of screen
	width = 320,
	--logical height of screen
	height = 480,
	--fps of application
	fps = 60,
	--for absolute positioning
	dx = application:getLogicalTranslateX() / application:getLogicalScaleX(),
	dy = application:getLogicalTranslateY() / application:getLogicalScaleY(),
}