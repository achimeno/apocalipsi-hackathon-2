--MENU
menu = gideros.class(Sprite)

function menu:init()
	application:setBackgroundColor(0x010522)

	local width = application:getContentWidth()
	local height = application:getContentHeight()
	
	--Background
	local background_texture = Texture.new("images/neon_earth.jpg")
	local background = Bitmap.new(background_texture)
	background:setScale(1)
	self:addChild(background)
	background:setPosition((application:getContentWidth()-background:getWidth())/2, (application:getContentHeight()-background:getHeight()))
	--background:setAnchorPoint(0.5,0.5)
	
	local label = TextField.new(fonts.Deathhead100, "Menu")
	label:setPosition((width-label:getWidth())/2, 100)
	label:setTextColor(0xffffff)
	self:addChild(label)
	
	local label = TextField.new(fonts.Deathhead100, "Start")
	label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("game", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	button2Game:setPosition((width-label:getWidth())/2, 250)	
	self:addChild(button2Game)
	
	
	local label = TextField.new(fonts.Deathhead50, "Millorar armament")
	label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("botiga", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	button2Game:setPosition((width-label:getWidth())/2, 300)	
	self:addChild(button2Game)
	
	--Comptador punts
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(20,10)
	self:addChild(coin)
	
	local label = TextField.new(fonts.Deathhead50 , sets.coins)
	label:setPosition(50,30)
	label:setTextColor(0xffffff)
	self:addChild(label)
	game.labelcoins = label
	


	-- define snow flakes (25 particles)
	local starsGFX = (Texture.new("images/meteorite.png"))
	flakes = CParticles.new(starsGFX, 30, 5.5, 5.5, "alpha")
	flakes:setDisplacement(320,0)
	flakes:setSpeed(50, 90)
	flakes:setSize(.3, 1)
	flakes:setRotation(0, -30, 360,30)
	flakes:setAlpha(20, 255)
	--flakes:setRotation(0, -20, 360, 20)
	flakes:setAlphaMorphIn(70, 2)
	flakes:setAlphaMorphOut(0, .5)
	flakes:setDirectionMorphIn(-60,5.4, 60, 5.4)
	flakes:setColor(255, 255,255)

	emitter_3 = CEmitter.new(110,0,80, self)
	---------------------------------------------------------------------
	-- assign particles to emitters
	
	emitter_3:assignParticles(flakes)
	emitter_3:start()

	--adds
	showAdd(5000)

	self:addEventListener("enterBegin", self.onTransitionInBegin, self)
	self:addEventListener("enterEnd", self.onTransitionInEnd, self)
	self:addEventListener("exitBegin", self.onTransitionOutBegin, self)
	self:addEventListener("exitEnd", self.onTransitionOutEnd, self)
end

function menu:onTransitionInBegin()
	print("menu - enter begin")
end

function menu:onTransitionInEnd()
	print("menu - enter end")
end

function menu:onTransitionOutBegin()
	print("menu - exit begin")
end

function menu:onTransitionOutEnd()
	print("menu - exit end")
end