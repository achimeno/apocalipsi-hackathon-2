--Utils
Utils = Core.class()

function Utils:buttonAPantalla(textureup, texturedown, pantalla)
	
	local up = Bitmap.new(textureup)
	local down = Bitmap.new(texturedown)

	-- create the button
	local button = Button.new(up, down)
	--start button on click event
	button:addEventListener("click", 
		function()
			--sound
			--sounds.play("hit")
			--go to url
			sceneManager:changeScene(pantalla, 0.5, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	
	return button
end