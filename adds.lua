-- if the platform is iOS, load the iAd module
if application:getDeviceInfo() == "iOS" then
    require "iad"
elseif application:getDeviceInfo() == "Android" then
	require "admob"
end

-- 4 event listeners for debug print
local function onBannerAdLoaded()
    print("banner ad loaded")
end

local function onBannerAdFailed(event)
    print("banner ad failed", event.errorCode, event.errorDescription)
end

local function onBannerActionBegin(event)
    print("banner action begin", event.willLeaveApplication)
end

local function onBannerActionFinished()
    print("banner action finished")
end

-- if iAd module is loaded and iAd framework is available, create an ad banner and then show it
local banner = nil
if iad and iad.isAvailable() then
    banner = iad.Banner.new(iad.Banner.BOTTOM, iad.Banner.PORTRAIT)
    banner:addEventListener(Event.BANNER_AD_LOADED, onBannerAdLoaded)
    banner:addEventListener(Event.BANNER_AD_FAILED, onBannerAdFailed)
    banner:addEventListener(Event.BANNER_ACTION_BEGIN, onBannerActionBegin)
    banner:addEventListener(Event.BANNER_ACTION_FINISHED, onBannerActionFinished)
    --banner:show()  -- show it because newly created banners are not visible by default
end

if admob then
	admob.loadAd(conf.adMob_Id, "banner")
	admob.setPosition(0, conf.height-75)
	admob.setVisible(false)
	
	local timerReloadAdd = Timer.new(conf.adMob_reload_time,1000)		
	timerReloadAdd:addEventListener(Event.TIMER_COMPLETE, 
		function()
			admob.loadAd(conf.adMob_Id, "banner")
			admob.setPosition(0, conf.height-75)
			admob.setVisible(false)
		end)
	timerReloadAdd:start()
end
-- show the banner (if it exists)
function showBanner()
    if banner ~= nil then
        banner:show()
    end
	if admob then
		admob.setVisible(true)
	end
end

-- hide the banner (if it exists)
function hideBanner()
    if banner ~= nil then
        banner:hide()
    end
	if admob then
		admob.setVisible(false)
	end
end

-- Temporized add
function showAdd(time)
	if ((iad and iad.isAvailable()) or admob) then
		showBanner()
		local timerOutAdd = Timer.new(time,1)
		
		timerOutAdd:addEventListener(Event.TIMER_COMPLETE, 
			function()
				hideBanner()
			end)
		timerOutAdd:start()
	end
end