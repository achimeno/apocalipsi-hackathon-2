--MENU
game = gideros.class(Sprite)

function game:init()

	--Background
	local background_texture = Texture.new("images/background_day.png")
	local background = Bitmap.new(background_texture)
	background:setScale(1.2)
	background:setPosition(-30, -20)
	self:addChild(background)

	ducksboard.guardians:push("{\"delta\": 1}")

	--create world instance
    self.world = b2.World.new(0, 5, true)
	
	--Cities
	local cases = {}
	for i = 1, 4, 1 do
		cases[i] = self:putCity((i-1)*80)
	end
	
	--Level
	local labelNivell = TextField.new(fonts.Deathhead100 , "Nivell "..sets.level)
	labelNivell:setPosition((conf.width-labelNivell:getWidth())/2,0)
	labelNivell:setTextColor(0xffffff)
	self:addChild(labelNivell)
	
	local label = TextField.new(fonts.Deathhead100, "Millorar armament")
	label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("shop", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	--button2Game:setPosition((width-label:getWidth())/2, 200)	
	--self:addChild(button2Game)

	local tween = GTween.new(labelNivell, 1, {y=200}, {delay=0, ease = easing.outBack, dispatchEvents = false})
	local tween = GTween.new(labelNivell, 1, {alpha=0}, {delay=1, ease = easing.outBack, dispatchEvents = true})
	tween:addEventListener("complete", 
		function()
			--Meteorits
			self.meteorits = {}
			local timer = Timer.new((100-sets.level*2)+math.random(500), sets.level*5)
			timer:addEventListener(Event.TIMER, 
				function()
					self.meteorits[#self.meteorits+1] = self:nouMeteorit(math.random(conf.width-50))
				end
			)
			timer:addEventListener(Event.TIMER_COMPLETE,
				function()
					--Espera a baixada meteorits
					local timer = Timer.new(3000, 1)
					timer:addEventListener(Event.TIMER_COMPLETE,
						function()			
							if(sets.coins > 0) then
								print("Nivell superat")
								self:pintaMenuNivelSuperat()
								--adds
								showAdd(5000)
							else
								
								self:pintaMort()
								sounds.play("fail")
							end
						end
					)
					timer:start()
				end
			)
			timer:start()
		end
	)

		
	
	--Duckboard
	game.punts = 0
	local timer = Timer.new(200, 1000000)
	timer:addEventListener(Event.TIMER, 
		function()
			local haenviar = game.punts
			if(haenviar>0) then
				ducksboard.comptador:push("{\"delta\": "..haenviar.."}")
				game.punts = game.punts - haenviar
			end
		end
	)
	 timer:start()
	
	--Pintar armament i defenses
	self:pintarArmament()
		
	self:addEventListener("enterBegin", self.onTransitionInBegin, self)
	self:addEventListener("enterEnd", self.onTransitionInEnd, self)
	self:addEventListener("exitBegin", self.onTransitionOutBegin, self)
	self:addEventListener("exitEnd", self.onTransitionOutEnd, self)
	
	--run world
    self:addEventListener(Event.ENTER_FRAME, self.onEnterFrame, self)
	--register contact events
	self.world:addEventListener(Event.BEGIN_CONTACT, self.onBeginContact, self)
	-- register for mouse events
	self:addEventListener(Event.MOUSE_DOWN, self.onMouseDown, self)
	
	local debugDraw = b2.DebugDraw.new()
	self.world:setDebugDraw(debugDraw)
--	stage:addChild(debugDraw)


	--Comptador punts
	local coin_texture = Texture.new("images/coin.png")
	local coin = Bitmap.new(coin_texture)
	coin:setScale(0.10)
	coin:setPosition(20,10)
	self:addChild(coin)
	
	local label = TextField.new(fonts.Deathhead50 , sets.coins)
	label:setPosition(50,30)
	label:setTextColor(0xffffff)
	self:addChild(label)
	game.labelcoins = label
	
	
	self:nevar()
	
	self:initExplosioMeteorits()
end

--running the world
function game:onEnterFrame() 
    -- edit the step values if required. These are good defaults!
    self.world:step(1/60, 8, 3)
    --iterate through all child sprites
	local remove = 0
    for i = 1, self:getNumChildren() do
        --get specific sprite
        local sprite = self:getChildAt(i)
        -- check if sprite HAS a body (ie, physical object reference we added)
        if sprite.body then
            --update position to match box2d world object's position
            --get physical body reference
            local body = sprite.body
            --get body coordinates
            local bodyX, bodyY = body:getPosition()
            --apply coordinates to sprite
            sprite:setPosition(bodyX, bodyY)
            --apply rotation to sprite
            sprite:setRotation(body:getAngle() * 180 / math.pi)
			
			--TOMA XAPUSA!!! 
			if(body.destruir and (not body.provadestruir or body.provadestruir == false)) then
				body.provadestruir = true
				body.destruir = false
				--self.world:destroyBody(body)
				body:setPosition(-100,700)
				remove = i;
			end
			body.provadestruir = false
        end
    end
	if remove > 0 then
		self:removeChildAt(remove)
	end
end

function game:onTransitionInBegin()
	print("game - enter begin")
	ducksboard.guardians:push("{\"delta\": 1}")
end

function game:onTransitionInEnd()
	print("game - enter end")
end

function game:onTransitionOutBegin()
	print("game - exit begin")
	ducksboard.guardians:push("{\"delta\": -1}")
end

function game:onTransitionOutEnd()
	print("game - exit end")
end

--define begin collision event handler function
function game:onBeginContact(e)
    --getting contact bodies
    local fixtureA = e.fixtureA
    local fixtureB = e.fixtureB
    local bodyA = fixtureA:getBody()
    local bodyB = fixtureB:getBody()
	
	if bodyA.type and bodyA.type == "city" then
		--print("Tocat")
		if(sets.armes[botiga.ESCUT_GROC]) then
			--Eliminar defensa
			GTween.new(self[botiga.ESCUT_GROC], 0.5, {alpha=0}, {delay=0, ease = easing.outBack, dispatchEvents = false})

			sets.armes[botiga.ESCUT_GROC] = false
		elseif(sets.armes[botiga.ESCUT_RED]) then
			--Eliminar defensa
			GTween.new(self[botiga.ESCUT_RED], 0.5, {alpha=0}, {delay=0, ease = easing.outBack, dispatchEvents = false})

			sets.armes[botiga.ESCUT_RED] = false
		else
			if(bodyA.destroyed == false) then
				bodyA.destroyed = true
				self:putCityBurning(bodyA.x)
				self:explosion(bodyA:getPosition())
				game:restaCoins(sets.level)
				sounds.play("blast")
			else
				game:restaCoins(sets.level)
			end
		end
		bodyB.destruir = true
	end

end

-- create a mouse joint on mouse down
function game:onMouseDown(event)
	if(self.meteorits and #self.meteorits>0) then 
		for i = 1, #self.meteorits,1 do
			if self.meteorits[i]:hitTestPoint(event.x, event.y) and self.meteorits[i].body.destruir == false then
				print("Meteorit tocat")
				self.meteorits[i].body.destruir = true
				--self.world:destroyBody(self.meteorits[i].body)
				--self.meteorits[i] = nil
				event:stopPropagation()
				
				game:sumaCoins(5)
				game.emitterExplosio:start()
				game.emitterExplosio:setPosition(event.x, event.y)
				sounds.play("blast")
			end
		end
	end
end

function game:putCity(x)
	local texture = Texture.new("images/city-icon.png")
	local city = Bitmap.new(texture)
	city:setScale(0.3)
	city:setPosition(x, (application:getContentHeight()-city:getHeight()))
	self:addChild(city)	
	city:setAnchorPoint(0.5, 0.5)
	--Fisica
	physicsAddBody(self.world, city, {type = "static", density = 1.0, friction = 0.5, bounce = 0.2})
	city.body.type = "city"
	city.body.destroyed = false
	city.body.x = x
	
	return city
end

function game:putCityBurning(x)
	local texture = Texture.new("images/city-icon_grey_burning.png")
	local city = Bitmap.new(texture)
	city:setScale(0.3)
	city:setPosition(x, (application:getContentHeight()-city:getHeight()))
	self:addChildAt(city,6)
	return city
end


function game:nouMeteorit(x)
	local texture = Texture.new("images/meteorite.png")
	local meteorit = Bitmap.new(texture)
	--city:setScale(1)
	meteorit:setAnchorPoint(0.5,0.5)
	meteorit:setPosition(x, -meteorit:getHeight()-100)
	self:addChild(meteorit)	
	
	--Fisica
	--local attributes = {type = "dynamic", density = 1.0, friction = 0.8, restitution = 0.1, radius = meteorit:getWidth()/2}
	--attributes.radius = meteorit:getWidth()/2		
	local body = self.world:createBody{type = b2.DYNAMIC_BODY}
	body:setPosition(meteorit:getX(), meteorit:getY())
	local circle = b2.CircleShape.new(0, 0, meteorit:getWidth()/2)
	local fixture = body:createFixture{shape = circle, d, density = 1.0, friction = 0.8, restitution = 0.1}
	meteorit.body = body
	meteorit.body.type = "meteorit"
	meteorit.body.destruir = false

	return meteorit
end

function game:sumaCoins(coins)
	sets.coins = sets.coins + coins
	game.punts = game.punts + coins
	game.labelcoins:setText(sets.coins)
end

function game:restaCoins(coins)
	sets.coins = sets.coins - coins
	game.labelcoins:setText(sets.coins)
end

function game:pintaMenuNivelSuperat()
	--Next level
	sets.level = sets.level + 1
	dataSaver.saveValue("sets", sets)
			
	local label = TextField.new(fonts.Deathhead100, "Nivell superat")
	label:setTextColor(0xffffff)
	label:setPosition((conf.width-label:getWidth())/2,70)
	self:addChild(label)
	
	local label = TextField.new(fonts.Deathhead100, "Next wave")
	label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()

			
			--sounds.play("hit")
			sceneManager:changeScene("game", 0, SceneManager.moveFromTop, easing.outBack)
			

		end
	, button)
	button2Game:setPosition((conf.width-label:getWidth())/2, 270)	
	self:addChild(button2Game)
	
	local label = TextField.new(fonts.Deathhead50, "Millorar armament")
	label:setTextColor(0xffffff)
	local button2Game = Button.new(label, label)
	button2Game:addEventListener("click", 
		function()
			--sounds.play("hit")
			sceneManager:changeScene("botiga", 0, SceneManager.moveFromTop, easing.outBack)
		end
	, button)
	button2Game:setPosition((conf.width-label:getWidth())/2, 300)	
	self:addChild(button2Game)

end

function game:pintaMort()

	
	--Background
	local background_texture = Texture.new("images/apocalipsis_transparencia.png")
	local background = Bitmap.new(background_texture)
	background:setScale(0)
	self:addChild(background)
	background:setPosition((application:getContentWidth()-background:getWidth())/2, application:getContentHeight())
	background:setAnchorPoint(0.5,0.5)
	
	--Reset game
	sets.level = 1
	sets.coins = 0
	dataSaver.saveValue("sets", sets)
	
	local tween = GTween.new(background, 4, {scaleX=3, scaleY=3}, {delay=0, ease = easing.outBack, dispatchEvents = true})
	tween:addEventListener("complete",
		function()
			local label = TextField.new(fonts.Deathhead100, "Apocalipsi!!")
			label:setTextColor(0xff0000)
			label:setPosition((conf.width-label:getWidth())/2,200)
			self:addChild(label)
	
			local timer = Timer.new(1000, 1)
			timer:addEventListener(Event.TIMER_COMPLETE,
				function()			
					sceneManager:changeScene("menu", 0, SceneManager.moveFromTop, easing.outBack)
				end
			)
			timer:start()		
		end
	)
end

function game:pintarArmament()
	--Pintar els tipus de defensa
	
	--De moment només la primera
	if(sets.armes[botiga.ESCUT_GROC]) then
		local texture = Texture.new("images/escut_groc.png")
		local escut = Bitmap.new(texture)
		escut:setScale(0.7)
		escut:setPosition(-50,350)
		self:addChild(escut)
		self[botiga.ESCUT_GROC] = escut
	end
	if(sets.armes[botiga.ESCUT_RED]) then
		local texture = Texture.new("images/escut_red.png")
		local escut = Bitmap.new(texture)
		escut:setScale(0.7)
		escut:setPosition(-50,350)
		self:addChild(escut)
		self[botiga.ESCUT_RED] = escut
	end
	if(sets.armes[botiga.LANZA_MISILES]) then
		--Launcher
		local launcher_texture = Texture.new("images/launcher.png")
		local launcher_bm = Bitmap.new(launcher_texture)
		local launcher = Button.new(launcher_bm, launcher_bm)
		launcher.disparat = false
		launcher:addEventListener("click", 
			function()
				if(launcher.disparat == false) then
					sounds.play("magic")
					launcher.disparat = true
					if(self.meteorits and #self.meteorits>0) then 
						for i = 1, #self.meteorits,1 do
							self.meteorits[i].body.destruir = true
							game:sumaCoins(1)
						end
					end
					sets.armes[botiga.LANZA_MISILES] = false
					GTween.new(self[botiga.LANZA_MISILES], 0.5, {alpha=0}, {delay=0, ease = easing.outBack, dispatchEvents = false})
				end
			end
		, button)

		launcher:setScale(0.2)
		launcher:setPosition((application:getContentWidth()-launcher:getWidth())/2, (application:getContentHeight()-launcher:getHeight()))
		self:addChild(launcher)
		self[botiga.LANZA_MISILES] = launcher
	end
	
end


function game:nevar()
	-- define snow flakes (25 particles)
	local starsGFX = (Texture.new("images/ember.png"))
	flakes = CParticles.new(starsGFX, 30, 5.5, 5.5, "alpha")
	flakes:setDisplacement(320,0)
	flakes:setSpeed(50, 90)
	flakes:setSize(.3, 1)
	flakes:setRotation(0, -30, 360,30)
	flakes:setAlpha(20, 255)
	--flakes:setRotation(0, -20, 360, 20)
	flakes:setAlphaMorphIn(70, 2)
	flakes:setAlphaMorphOut(0, .5)
	flakes:setDirectionMorphIn(-60,5.4, 60, 5.4)
	flakes:setColor(255, 255,255)

	emitter_3 = CEmitter.new(110,0,80, self)
	---------------------------------------------------------------------
	-- assign particles to emitters
	
	emitter_3:assignParticles(flakes)
	emitter_3:start()

end

function game:explosion(x, y)
	local particleGFX = (Texture.new("images/explosion.png"))
	fire = CParticles.new(particleGFX, 30, 1.5, 1.5, "add")
	fire:setSpeed(220, 320)
	fire:setSize(5, 10)
	fire:setColor(255,110,80)
	fire:setAlpha(0)
	fire:setRotation(0, -160, 360, 160)
	fire:setAlphaMorphIn(240, .15)
	fire:setAlphaMorphOut(50, .9)
	fire:setSizeMorphOut(0.2, 0.9)
	fire:setSpeedMorphOut(450, 1, 450, 0.5)
	fire:setColorMorphOut(110,110,110,1.2,20,20,20,1.5)

	emitter_3 = CEmitter.new(x,y, -90, self)
	---------------------------------------------------------------------
	-- assign particles to emitters
	
	emitter_3:assignParticles(fire)
	emitter_3:start()

end

function game:initExplosioMeteorits()
	local waveGFX = (Texture.new("images/shockwave1.png"))	
	local sparklesGFX = (Texture.new("images/exp1.png"))	
	local smokeGFX = (Texture.new("images/smoke.png"))	
	---------------------------------------------------------------------
	-- define explosion weave
	wave = CParticles.new(waveGFX, 1, 1, 0, "add")
	wave:setSize(0.2)
	wave:setAlpha(190)
	wave:setColor(150,120,155)
	wave:setAlphaMorphOut(0, 1)
	wave:setSizeMorphOut(2, 1)

	---------------------------------------------------------------------
	-- define explosion sparkles
	sparkles = CParticles.new(sparklesGFX, 10, 1, 0, "add")
	sparkles:setSize(0.3, 0.8)
	sparkles:setSpeed(150, 350)
	sparkles:setAlpha(0)
	sparkles:setDirection(0, 360)
	sparkles:setColor(255,100,190)
	sparkles:setAlphaMorphIn(255, .5)
	sparkles:setAlphaMorphOut(0, .5)

	---------------------------------------------------------------------
	-- define explosion sparkles
	smoke = CParticles.new(smokeGFX, 5, 1, 0, "add")
	smoke:setSize(1, 2)
	smoke:setSpeed(100, 160)
	smoke:setRotation(0, 80, 360, -80)
	smoke:setDirection(0, 360)
	smoke:setColor(120,90,150)
	smoke:setAlphaMorphOut(0, 1)
	smoke:setSizeMorphOut(3, 1)

	---------------------------------------------------------------------
	-- set emitter 1 (sx) and emitter 2 (dx)

	emitter_1 = CEmitter.new(160,240,0, self)


	---------------------------------------------------------------------
	-- assign particles to emitters

	wave:setLoopMode(1)
	sparkles:setLoopMode(1)
	smoke:setLoopMode(1)

	emitter_1:assignParticles(wave)
	emitter_1:assignParticles(sparkles)
	emitter_1:assignParticles(smoke)
	game.emitterExplosio = emitter_1
end