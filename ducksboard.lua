--Ducksboard
Ducksboard = Core.class()

Ducksboard.apikey = "e62nlqnC0GkO65e3HYteR9c7gQJPDqzHwNWkns0yRsjfEKC8M4"
Ducksboard.url = "https://push.ducksboard.com/v/" --95177"

function Ducksboard:init(idwitget)
	self.url = Ducksboard.url..idwitget
	
	--local base64 = Base64.new()
	self.authorization = "Basic "..Base64:enc(Ducksboard.apikey..":x")
end

function Ducksboard:push(body)

	local headers = {
	["Authorization"] = self.authorization, 
	["Content-Type"]  = "application/x-www-form-urlencoded",
	["Content-Length"] = tostring(body:len()),
	["Connection"] = "close",
	}
 
	local loader = UrlLoader.new(self.url, UrlLoader.POST, headers, body)
 
	local function onComplete(event)
		print(event.data)
		print("Urlloader complete")
	end
 
	local function onError()
		print("error")
		end
 
	local function onProgress(event)
		print("progress: " .. event.bytesLoaded .. " of " .. event.bytesTotal)
	end
 
	loader:addEventListener(Event.COMPLETE, onComplete)
	loader:addEventListener(Event.ERROR, onError)
	loader:addEventListener(Event.PROGRESS, onProgress)
 
end