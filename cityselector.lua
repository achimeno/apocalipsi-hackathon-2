--MENU
cityselector = gideros.class(Sprite)

function cityselector:init()


	self:addEventListener("enterBegin", self.onTransitionInBegin, self)
	self:addEventListener("enterEnd", self.onTransitionInEnd, self)
	self:addEventListener("exitBegin", self.onTransitionOutBegin, self)
	self:addEventListener("exitEnd", self.onTransitionOutEnd, self)
end

function cityselector:onTransitionInBegin()
	print("cityselector - enter begin")
end

function cityselector:onTransitionInEnd()
	print("cityselector - enter end")
end

function cityselector:onTransitionOutBegin()
	print("cityselector - exit begin")
end

function cityselector:onTransitionOutEnd()
	print("cityselector - exit end")
end