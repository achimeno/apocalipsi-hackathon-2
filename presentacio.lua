--PRESENTACIÓ
presentacio = gideros.class(Sprite)

function presentacio:init()
	local width = application:getContentWidth()
	local height = application:getContentHeight()
	
	application:setBackgroundColor(0x000000)

	--Background
	local background_texture = Texture.new("images/apocalipsis.jpg")
	local background = Bitmap.new(background_texture)
	background:setScale(0.2)
	self:addChild(background)
	background:setPosition((application:getContentWidth()-background:getWidth())/2, (application:getContentHeight()-background:getHeight()))
	background:setAnchorPoint(0.5,0.5)


	-- create a label title
	local label = TextField.new(fonts.Deathhead100 , "Apocalipsi")
	label:setPosition((width-label:getWidth())/2, height/2)
	label:setTextColor(0xffffff)
	self:addChild(label)
	

	local tween = GTween.new(background, 2, {scaleX=0.3, scaleY=0.3}, {delay=0, ease = easing.outBack, dispatchEvents = false})
	
	local tween = GTween.new(label, 0.5, {alpha=1}, {delay=1, ease = easing.outBack, dispatchEvents = true})
	tween:addEventListener("complete",
		function()
			sceneManager:changeScene("menu", 1, SceneManager.fade, easing.outBack) 
		end
	)
	
	self:addEventListener("enterBegin", self.onTransitionInBegin, self)
	self:addEventListener("enterEnd", self.onTransitionInEnd, self)
	self:addEventListener("exitBegin", self.onTransitionOutBegin, self)
	self:addEventListener("exitEnd", self.onTransitionOutEnd, self)
end

function presentacio:onTransitionInBegin()
	print("presentacio - enter begin")
end

function presentacio:onTransitionInEnd()
	print("presentacio - enter end")
end

function presentacio:onTransitionOutBegin()
	print("presentacio - exit begin")
end

function presentacio:onTransitionOutEnd()
	print("presentacio - exit end")
end